This README explains you how to use our project. Note that the project doesn't have a functional graphic interface so you have to compile each element separately one from another.

# PROJECT #

Use "make all" to compile the whole project.

## PreTreatment  ##

Use "make PreTreatment" if you didn't use "make all".

./PreTreatment [-bfg] [-a] [-r angle] [image]

-b binarizes the image

-f filters your image

-g puts your image in grayscale

-a does all the above

-r angle rotates your image with the angle you specified

## Segmentation ##

Use "make Segmentation" if you didn't use "make all"

./Segmentation [image]

## Graphic Interface ##

Use "make Gtk" if you didn't use "make all"

./Gtk opens a windows where you can load an image and work on it.


# CONTACTS #

If you have any question, feel free to follow the link : http://epitapocryphe.wordpress.com/ where you can find our reports and the ways to contact us.