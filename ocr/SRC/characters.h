//Libère la mémoire d'un matrice.
void freeMatrix(size_t sizeX, int **src);

//Redimensionne une matrice en 16x16.
int **resize(size_t sizeX, size_t sizeY, int **src);

//Enleve la ligne i du caractère.
void removeLine(int **inputs, size_t line, size_t *sizeX);

//Enleve les lignes blanches (inutiles) du caractère.                        
void removeWhiteLines(int **inputs, size_t *sizeX, size_t *sizeY);

//Enleve la colonne i du caractère.
void removeColomn(int **inputs, size_t colomn, size_t sizeX, size_t *sizeY);

//Enleve les colonnes blanches (inutiles) du caractères.
void removeWhiteColomns(int **inputs, size_t *sizeX, size_t *sizeY);

//Récupère les taux de pixels noir sur les lignes.
float *averageLines(int **inputs);

//Récupère les taux de pixels noir sur les colonnes.
float *averageColumns(int **inputs);

//Ajustement total du caractère.
int **adjustingChar(size_t sizeX, size_t sizeY, int **inputs);

//Reconnait un caractère (avec apprentissage ou pas).
char recognizeChar(Network *n, size_t sizeX, size_t sizeY, int **inputs,
		        float result, int learnOn);
