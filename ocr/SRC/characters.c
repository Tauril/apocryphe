/*****************************INCLUDES*****************************************/

#include <stdio.h>
#include <stdlib.h>
#include "network.h"

/******************************DEFINES*****************************************/

#define SIZE	16

/******************************FUNCTIONS***************************************/

//Int to float
int intToFloat(float f)
{
	if (f - (int)f > 0.5)
		return (int)(f+1);
	else
		return (int)f;
}

//Libère la mémoire d'une matrice.
void freeMatrix(size_t sizeX, int **src)
{
	for(size_t i = 0; i < sizeX; i++)
		free(src[i]);

	free(src);
}

//Redimensionne une matrice en 16x16.
int **resize(size_t sizeX, size_t sizeY, int **src)
{
	int **output = malloc(sizeof(int*) * SIZE);

	for (int i = 0; i < SIZE; i++)
		output[i] = malloc(sizeof(int) * SIZE);

	size_t i, j;
	int x, y, minx, miny, maxx, maxy;

	for (i = 0; i < sizeX; i++)
	{
		for (j = 0; j < sizeY; j++)
		{
			minx = i * SIZE / sizeX;
			miny = j * SIZE / sizeY;

			maxx = minx + (int)(SIZE / sizeX);
			if (maxx >= SIZE)
				maxx--;
			maxy = miny + (int)(SIZE / sizeY);
			if (maxy >= SIZE)
				maxy--;

			for (x = minx; x <= maxx; x++)
			{
				for (y = miny; y <= maxy; y++)
					output[x][y] = src[i][j];
			}
		}
	}

	return output;
}

//Enleve la ligne i du caractère.
void removeLine(int **inputs, size_t line, size_t *sizeX)
{
	for (size_t i = line + 1; i < *sizeX; i++)
		inputs[i - 1] = inputs[i];

	(*sizeX)--;
}

//Enleve les lignes blanches (inutiles) du caractère.
void removeWhiteLines(int **inputs, size_t *sizeX, size_t *sizeY)
{
	int isWhite;

	for(size_t i = 0; i < *sizeX; i++)
	{
		isWhite = 1;

		for(size_t j = 0; j < *sizeY; j++)
		{
			if (inputs[i][j])
				isWhite = 0;
		}

		if (isWhite)
		{
			removeLine(inputs, i, sizeX);
			i = 0;
		}
	}
}

//Enleve la colonne i du caractère.
void removeColomn(int **inputs, size_t colomn, size_t sizeX, size_t *sizeY)
{
	for(size_t i = 0; i < sizeX; i++)
	{
		for(size_t j = colomn + 1; j < *sizeY; j++)
			inputs[i][j - 1] = inputs[i][j];
	}

	(*sizeY)--;
}

//Enleve les colonnes blanches (inutiles) du caractères.
void removeWhiteColomns(int **inputs, size_t *sizeX, size_t *sizeY)
{
	int isWhite;

	for (size_t j = 0; j < *sizeY; j++)
	{
		isWhite = 1;

		for(size_t i = 0; i < *sizeX; i++)
		{
			if (inputs[i][j])
				isWhite = 0;
		}

		if (isWhite)
		{
			removeColomn(inputs, j, *sizeX, sizeY);
			j = 0;
		}
	}
}

//Récupère les taux de pixels noir sur les lignes.
float *averageLines(int **inputs)
{
	float *output = malloc(sizeof(float) * SIZE);

	for(int i = 0; i < SIZE; i++)
	{
		output[i] = 0;

		for(int j = 0; j < SIZE; j++)
			output[i] += inputs[i][j];

		output[i] /= SIZE;
	}

	return output;
}

//Récupère les taux de pixels noir sur les colonnes.
float *averageColumns(int **inputs)
{
	float *output = malloc(sizeof(float) * SIZE);

	for (int j = 0; j < SIZE; j++)
	{
		output[j] = 0;

		for (int i = 0; i < SIZE; i++)
			output[j] += inputs[i][j];

		output[j] /= SIZE;
	}

	return output;
}

//Ajustement total du caractère.
int **adjustingChar(size_t sizeX, size_t sizeY, int **inputs)
{
	removeWhiteLines(inputs, &sizeX, &sizeY);
	removeWhiteColomns(inputs, &sizeX, &sizeY);

	return (resize(sizeX, sizeY, inputs));
}

//Reconnait un caractère (avec apprentissage ou pas).
char recognizeChar(Network *n, size_t sizeX, size_t sizeY, int **inputs,
		float result, int learnOn)
{
	int **adjustedChar = adjustingChar(sizeX, sizeY, inputs);
	int isSpace = 1;

	int *vect = malloc(sizeof(int) * SIZE * SIZE);

	printf("Detected character (matrix format):\n");

	for (size_t i = 0; i < SIZE; i++)
	{
		for (size_t j = 0; j < SIZE; j++)
		{
			printf("%d", adjustedChar[i][j]);
			vect[(i * SIZE) + j] = adjustedChar[i][j];
			if (adjustedChar[i][j])
				isSpace = 0;
		}

		printf("\n");
	}

//	printf("\n");

	if (isSpace)
		return -2;

	float **averages = malloc(sizeof(float*) * 2);
	averages[0] = averageLines(adjustedChar);
	averages[1] = averageColumns(adjustedChar);

	setInputs(n, vect);

	if (learnOn)
		return learnNetwork(n, result);
	else
		return runNetwork(n);
}
