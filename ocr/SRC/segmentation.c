#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "segmentation.h"
#include <stdbool.h>
#include <err.h>
#include "network.h"
#include "characters.h"

/********************************** STACK ************************************/

struct stack
{
    struct stack *next;
    int value;
};

struct stack *new_stack()
{
    return NULL;
}

int is_empty_stack(struct stack *s)
{
    return (s == NULL);
}

struct stack *add_stack(int elt, struct stack *s)
{
    struct stack *new = malloc(sizeof(struct stack));
    new->value = elt;
    new->next = s;
    return new;
}

int first_stack(struct stack *s)
{
    return (s->value);
}

struct stack *remove_stack(struct stack *s)
{
    struct stack *rm = s;
    s = s->next;
    free(rm);
    return s;
}

/************************ FONCTIONS D'INITIALISATION *************************/

void wait_for_keypressed(void)
{
    SDL_Event event;
    for (;;)
    {
        SDL_PollEvent(&event);
        switch (event.type)
        {
            case SDL_KEYDOWN: return;
            default: break;
    }
  }
}

void init_sdl(void)
{
  if(SDL_Init(SDL_INIT_VIDEO)==-1)
        errx(1,"Could not initialize SDL: %s.\n", SDL_GetError());
}

SDL_Surface* display_image(SDL_Surface *img)
{
    SDL_Surface *screen;

    screen = SDL_SetVideoMode(img->w, img->h, 0, SDL_SWSURFACE|SDL_ANYFORMAT);
    if (screen == NULL)
    {
        errx(1, "Couldn't set %dx%d video mode: %s\n",
         img->w, img->h, SDL_GetError());
    }

    if(SDL_BlitSurface(img, NULL, screen, NULL) < 0)
        warnx("BlitSurface error: %s\n", SDL_GetError());

    SDL_UpdateRect(screen, 0, 0, img->w, img->h);

    wait_for_keypressed();

    return screen;
}

/******************************* SOUS-FONCTIONS ******************************/

Uint32 getPixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8*)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
        case 1:
            return *p;
            break;

        case 2:
            return *(Uint16*)p;
            break;

        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
                return p[0] << 16 | p[1] << 8 | p[2];
            else
                return p[0] | p[1] << 8 | p[2] << 16;
            break;

        case 4:
            return *(Uint32*)p;
            break;

        default:
            return 0;
    }
}

void setPixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8*)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
        case 1:
            *p = pixel;
            break;

        case 2:
            *(Uint16 *)p = pixel;
            break;

        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
                p[0] = (pixel >> 16) & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = pixel & 0xff;
            }
            else {
                p[0] = pixel & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = (pixel >> 16) & 0xff;
            }
            break;

        case 4:
            *(Uint32 *)p = pixel;
            break;
    }
}

int pixelColor(Uint32 pixel, SDL_Surface *surface)
{
    SDL_LockSurface(surface);

    Uint8 r,g,b;

    SDL_GetRGB(pixel, surface->format, &r, &g, &b);

    if (r < 224 && g < 224 && b < 224)
    {
        SDL_UnlockSurface(surface);
        return 0;
    }
    else
    {
        SDL_UnlockSurface(surface);
        return 255;
    }
}

void becomeBlack(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    SDL_LockSurface(surface);

    pixel = getPixel(surface, x, y);
    pixel = SDL_MapRGB(surface->format, 0, 0, 0);
    setPixel(surface, x, y, pixel);

    SDL_UnlockSurface(surface);
}

void becomeWhite(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    SDL_LockSurface(surface);

    pixel = getPixel(surface, x, y);
    pixel = SDL_MapRGB(surface->format, 255, 255, 255);
    setPixel(surface, x, y, pixel);

    SDL_UnlockSurface(surface);
}

void becomeGreen(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    SDL_LockSurface(surface);

    pixel = getPixel(surface, x, y);
    pixel = SDL_MapRGB(surface->format, 0, 250, 154);
    setPixel(surface, x, y, pixel);

    SDL_UnlockSurface(surface);
}

void becomeBlue(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    SDL_LockSurface(surface);

    pixel = getPixel(surface, x, y);
    pixel = SDL_MapRGB(surface->format, 0, 0, 255);
    setPixel(surface, x, y, pixel);

    SDL_UnlockSurface(surface);
}

int space_average_seg_x(SDL_Surface *surface, int y)
{
    SDL_LockSurface(surface);

    Uint32 pixel;
    Uint32 pixel2;
    int average = 0;
    int spaceNumber = 0;
    bool countWhitePixel = false;

    for (int x = 0; x < surface->w - 1; x++)
    {
        pixel = getPixel(surface, x, y);
        pixel2 = getPixel(surface, (x+1), y);

        if ((pixelColor(pixel, surface) == 0) && (pixelColor(pixel2, surface)
                                                                    == 255))
            countWhitePixel = true;

        if ((pixelColor(pixel, surface) == 255) &&
                                        ((pixelColor(pixel2, surface) == 0)))
        {
            countWhitePixel = false;
            spaceNumber += 1;
            average += 1;
        }

        if (countWhitePixel && pixelColor(pixel, surface) == 255)
        {
            average += 1;
        }
    }

    if (spaceNumber != 0)
        average = (average / spaceNumber);

    SDL_UnlockSurface(surface);

    return (average);
}

int space_average_seg_y(SDL_Surface *surface, int x)
{
    SDL_LockSurface(surface);

    Uint32 pixel;
    Uint32 pixel2;
    int average = 0;
    int spaceNumber = 0;
    bool countWhitePixel = false;

    for (int y = 0; y < surface->h - 1; y++)
    {
        pixel = getPixel(surface, x, y);
        pixel2 = getPixel(surface, x, (y+1));

        if ((pixelColor(pixel, surface) == 0) && (pixelColor(pixel2, surface)
                                                                    == 255))
            countWhitePixel = true;

        if ((pixelColor(pixel, surface) == 255) &&
                                        ((pixelColor(pixel2, surface) == 0)))
        {
            countWhitePixel = false;
            spaceNumber += 1;
            average += 1;
        }

        if (countWhitePixel && pixelColor(pixel, surface) == 255)
        {
            average += 1;
        }
    }

    if (spaceNumber != 0)
        average = (average / spaceNumber);

    SDL_UnlockSurface(surface);

    return (average);
}

int *rec_coord(SDL_Surface *surface, int x, int y, int *coord, int **tab)
{
    struct stack *s = new_stack();
    int i, j;
    Uint32 pixel;

    s = add_stack(x, s);
    s = add_stack(y, s);

    while (!(is_empty_stack(s)))
    {
        j = first_stack(s);
        s = remove_stack(s);
        i = first_stack(s);
        s = remove_stack(s);

        pixel = getPixel(surface, i, j);

        if (i > 0 && i < surface->w - 1 && j > 0 && j < surface->h - 1
                            && !(tab[i][j]) && pixelColor(pixel, surface) == 0)
        {
            tab[i][j] = 1;

            if (coord[0] > i)
                coord[0] = i;

            if (coord[1] > j)
                coord[1] = j;

            if (coord[2] < i)
                coord[2] = i;

            if (coord[3] < j)
                coord[3] = j;

            s = add_stack(i+1, s);
            s = add_stack(j, s);

            s = add_stack(i-1, s);
            s = add_stack(j, s);

            s = add_stack(i, s);
            s = add_stack(j+1, s);

            s = add_stack(i, s);
            s = add_stack(j-1, s);
        }
    }

    return (coord);
}

int isWhiteColumnPS(SDL_Surface *surface, int w, int x, int **line)
{
    SDL_LockSurface(surface);

    Uint32 pixel;
    bool whiteColumnPS = false;
    int nbBlackPixel = 0;

    for (int y = line[w][1]; y <= line[w][3]; y++)
    {
        pixel = getPixel(surface, x, y);

        if (y != line[w][1] && y != line[w][3] && pixelColor(pixel, surface)
                                                                        == 0)
            nbBlackPixel += 1;

        if (y == line[w][3] && nbBlackPixel > 0)
            whiteColumnPS = false;
        else if (y == line[w][3] && nbBlackPixel == 0)
            whiteColumnPS = true;
    }

    if (whiteColumnPS)
    {
        SDL_UnlockSurface(surface);
        return 1;
    }
    else
    {
        SDL_UnlockSurface(surface);
        return 0;
    }
}

int space_average(SDL_Surface *surface, int **line, int numberLine)
{
    SDL_LockSurface(surface);

    Uint32 pixel;
    int average = 0;
    int spaceNumber = 0;
    int y = (line[numberLine][3] / 2);
    bool countWhitePixel = false;

    for (int x = line[numberLine][0]; x <= line[numberLine][2]; x++)
    {
        pixel = getPixel(surface, x, y);

        if (!(isWhiteColumnPS(surface, numberLine, x, line)) &&
                        (isWhiteColumnPS(surface, numberLine, (x+1), line)))
            countWhitePixel = true;

        if ((isWhiteColumnPS(surface, numberLine, x, line)) &&
                        (!(isWhiteColumnPS(surface, numberLine, (x+1), line))))
        {
            countWhitePixel = false;
            spaceNumber += 1;
            average += 1;
        }

        if (countWhitePixel && pixelColor(pixel, surface) == 255)
        {
            average += 1;
        }
    }

    if (spaceNumber != 0)
        average = (average / spaceNumber);

    SDL_UnlockSurface(surface);

    return (average);
}

/***************************** CREATION DES BLOCS ****************************/

void seg_block(SDL_Surface *surface, int **tabH, int **tabV)
{
    SDL_LockSurface(surface);

    Uint32 pixel;
    int tab[surface->w][surface->h];

// COMBINATION DES DEUX TABLEAUX BINAIRES

    for (int x = 0; x < surface->w; x++)
    {
        for (int y = 0; y < surface->h; y++)
        {
            tab[x][y] = (tabV[x][y] & tabH[x][y]);
        }
    }

// MISE A JOUR DE L'IMAGE

    for (int y = 0; y < surface->h; y++)
    {
        for (int x = 0; x < surface->w; x++)
        {
            pixel = getPixel(surface, x, y);

            if (tab[x][y] == 1)
                becomeBlack(surface, x, y, pixel);
            else if (tab[x][y] == 0)
                becomeWhite(surface, x, y, pixel);
        }
    }

    SDL_UnlockSurface(surface);
}

/************************** RLSA ALGORITHM HORIZONTAL ************************/

int **horizontal_detect(SDL_Surface *surface)
{
    SDL_LockSurface(surface);

    Uint32 pixel;
    int nbWhitePixel = 0;
    bool countWhitePixel = false;
    int th = 0;

// INITIALISATION DU TABLEAU A DEUX DIMENSIONS

    int **tabH;
    tabH = malloc(sizeof(int*));
    for (int i = 0; i < surface->w; i++)
    {
        tabH = realloc(tabH, sizeof(int*) * (i + 1));
        tabH[i] = malloc(sizeof(int) * (surface->h));
    }

// APPLICATION DE L'ALGORITHME

    for (int y = 0; y < surface->h; y++)
    {
        th = space_average_seg_x(surface, y);
        nbWhitePixel = 0;
        countWhitePixel = false;

        for (int x = 0; x < surface->w; x++)
        {
            pixel = getPixel(surface, x, y);
            if (pixelColor(pixel, surface) == 0)
            {
                countWhitePixel = true;

                if (nbWhitePixel <= th && nbWhitePixel != 0)
                {
                    for (int k = x - nbWhitePixel; k < x; k++)
                    {
                        pixel = getPixel(surface, k, y);
                        becomeBlack(surface, k, y, pixel);
                    }
                }
                nbWhitePixel = 0;
            }
            else if (pixelColor(pixel, surface) == 255 && countWhitePixel)
                nbWhitePixel += 1;
        }
    }

// MISE A JOUR DE L'IMAGE

    for (int y = 0; y < surface->h; y++)
    {
        for (int x = 0; x < surface->w; x++)
        {
            pixel = getPixel(surface, x, y);

            if (pixelColor(pixel, surface) == 0)
                tabH[x][y] = 1;
            else if (pixelColor(pixel, surface) == 255)
                tabH[x][y] = 0;
        }
    }

    SDL_UnlockSurface(surface);

    return (tabH);
}

/************************** RLSA ALGORITHM VERTICAL **************************/

int **vertical_detect(SDL_Surface *surface)
{
    SDL_LockSurface(surface);

    Uint32 pixel;
    int nbWhitePixel = 0;
    int tv = 0;
    bool countWhitePixel = false;

// INITIALISATION DU TABLEAU A DEUX DIMENSIONS

    int **tabV;
    tabV = malloc(sizeof(int*));
    for (int i = 0; i < surface->w; i++)
    {
        tabV = realloc(tabV, sizeof(int*) * (i + 1));
        tabV[i] = malloc(sizeof(int) * (surface->h));
    }

// APPLICATION DE L'ALGORITHME

    for (int x = 0; x < surface->w; x++)
    {
        tv = space_average_seg_y(surface, x);
        nbWhitePixel = 0;
        countWhitePixel = false;

        for (int y = 0; y < surface->h; y++)
        {
            pixel = getPixel(surface, x, y);
            if (pixelColor(pixel, surface) == 0)
            {
                countWhitePixel = true;

                if (nbWhitePixel <= tv && nbWhitePixel != 0)
                {
                    for (int k = y - nbWhitePixel; k < y; k++)
                    {
                        pixel = getPixel(surface, x, k);
                        becomeBlack(surface, x, k, pixel);
                    }
                }
                nbWhitePixel = 0;
            }
            else if (pixelColor(pixel, surface) == 255 && countWhitePixel)
                nbWhitePixel += 1;
        }
    }

// MISE A JOUR DE L'IMAGE

    for (int y = 0; y < surface->h; y++)
    {
        for (int x = 0; x < surface->w; x++)
        {
            pixel = getPixel(surface, x, y);
            if (pixelColor(pixel, surface) == 0)
                tabV[x][y] = 1;
            else if (pixelColor(pixel, surface) == 255)
                tabV[x][y] = 0;
        }
    }

    SDL_UnlockSurface(surface);

    return (tabV);
}

/**************************** DETECTION DES BLOCS ****************************/

int **block_detect(SDL_Surface *surface, int *numberBlock)
{
    SDL_LockSurface(surface);

    Uint32 pixel;
    int x_min = 0;
    int y_min = 0;
    int x_max = 0;
    int y_max = 0;
    int **block; //matrice contenant les coordonées de chaque bloc
    block = malloc(sizeof(int*));
    block[0] = malloc(sizeof(int) * 4);

    block[0][0] = x_min;
    block[0][1] = y_min;
    block[0][2] = x_max;
    block[0][3] = y_max;

    int *coord; //tableau contenant les coordonnées min et max des voisins
    coord = malloc(sizeof(int) * 4);

    coord[0] = 0;
    coord[1] = 0;
    coord[2] = 0;
    coord[3] = 0;

    int **tab; //matrice permettant de savoir si la case a déjà été visitée
    tab = malloc(sizeof(int*) * (surface->w));
    for (int i = 0; i < surface->w; i++)
    {
        tab[i] = malloc(sizeof(int) * (surface->h));
    }

    for (int i = 0; i < surface->w; i++)
    {
        for (int j = 0; j < surface->h; j++)
        {
            tab[i][j] = 0;
        }
    }

    for (int y = 0; y < surface->h; y++) //Coordonnées des blocs
    {
        for (int x = 0; x < surface->w; x++)
        {
            pixel = getPixel(surface, x, y);

            if (!(tab[x][y]) && pixelColor(pixel, surface) == 0)
            {
                *numberBlock += 1;
                x_min = 0;
                x_max = 0;
                y_min = 0;
                y_max = 0;
            }

            if (pixelColor(pixel, surface) == 0)
            {
                if (!(tab[x][y]))
                {
                    block = realloc(block, sizeof(int*) * (*numberBlock + 1));
                    block[*numberBlock] = malloc(sizeof(int) * 4);

                    x_min = x;
                    block[*numberBlock][0] = x_min;
                    coord[0] = x_min;

                    y_min = y;
                    block[*numberBlock][1] = y_min;
                    coord[1] = y_min;

                    x_max = x;
                    block[*numberBlock][2] = x_max;
                    coord[2] = x_max;

                    y_max = y;
                    block[*numberBlock][3] = y_max;
                    coord[3] = y_max;

                    rec_coord(surface, x, y, coord, tab);

                    if (x_min > coord[0])
                    {
                        x_min = coord[0];
                        block[*numberBlock][0] = x_min;
                    }
                    if (x_min > x)
                    {
                        x_min = x;
                        block[*numberBlock][0] = x_min;
                    }

                    if (y_min > coord[1])
                    {
                        y_min = coord[1];
                        block[*numberBlock][1] = y_min;
                    }
                    if (y_min > y)
                    {
                        y_min = y;
                        block[*numberBlock][1] = y_min;
                    }

                    if (x_max < coord[2])
                    {
                        x_max = coord[2];
                        block[*numberBlock][2] = x_max;
                    }
                    if (x_max < x)
                    {
                        x_max = x;
                        block[*numberBlock][2] = x_max;
                    }

                    if (y_max < coord[3])
                    {
                        y_max = coord[3];
                        block[*numberBlock][3] = y_max;
                    }
                    if (y_max < y)
                    {
                        y_max = y;
                        block[*numberBlock][3] = y_max;
                    }

                    tab[x][y] = 1;
                }
            }
        }
    }

    for (int i = 1; i <= *numberBlock; i++) //resume les coordonnées
    {
        printf("| %d. x_min = %d | y_min = %d | x_max = %d | y_max = %d |\n",
                        i, block[i][0], block[i][1], block[i][2], block[i][3]);
    }
    printf("\n");

    for (int w = 1; w <= *numberBlock; w++) //encadre chaque bloc
    {
        for (int y = block[w][1]; y <= block[w][3]; y++)
        {
            for (int x = block[w][0]; x <= block[w][2]; x++)
            {
                pixel = getPixel(surface, x, y);
                if (x == block[w][0] || x == block[w][2] || y == block[w][1]
                                                        || y == block[w][3])
                {
                    becomeGreen(surface, x, y, pixel);
                }
            }
        }
    }

    SDL_UnlockSurface(surface);

    return (block);
}

/**************************** DETECTION DES LIGNES ***************************/

int **line_detect(SDL_Surface *surface, int *numberLine, int **block, int
                                                                *numberBlock)
{
    SDL_LockSurface(surface);

    Uint32 pixel;
    bool whiteLine = true;
    bool newBlock = true;
    int nbBlackPixel = 0;
    int x_min = 0;
    int y_min = 0;
    int x_max = 0;
    int y_max = 0;
    int **line;
    line = malloc(sizeof(int*));
    line[0] = malloc(sizeof(int) * 4);

    line[0][0] = x_min;
    line[0][1] = y_min;
    line[0][2] = x_max;
    line[0][3] = y_max;

// CALCUL DES COORDONNEES DE CHAQUE LIGNE

    for (int w = 1; w <= *numberBlock; w++)
    {
        newBlock = true;

        for (int y = block[w][1]; y <= block[w][3]; y++)
        {
            nbBlackPixel = 0;

            for (int x = block[w][0]; x <= block[w][2]; x++)
            {
                pixel = getPixel(surface, x, y);

                if (pixelColor(pixel, surface) == 0)
                    nbBlackPixel += 1;

                if (x == block[w][2] && nbBlackPixel == 0)
                    whiteLine = true;
                else if (x == block[w][2] && nbBlackPixel > 0)
                    whiteLine = false;

                if ((whiteLine || newBlock) && pixelColor(pixel, surface) == 0)
                {
                    *numberLine += 1;
                    x_min = 0;
                    x_max = 0;
                    y_min = 0;
                    y_max = 0;
                }

                if (pixelColor(pixel, surface) == 0)
                {
                    if (whiteLine || newBlock)
                    {
                        line = realloc(line, sizeof(int*) * (*numberLine + 1));
                        line[*numberLine] = malloc(sizeof(int) * 4);

                        x_min = x;
                        line[*numberLine][0] = x_min;

                        y_min = y;
                        line[*numberLine][1] = y_min;

                        newBlock = false;
                    }

                    if (x_min > x)
                    {
                        x_min = x;
                        line[*numberLine][0] = x_min;
                    }
                    else if (x_max < x)
                    {
                        x_max = x;
                        line[*numberLine][2] = x_max;
                    }

                    if (y_max < y)
                    {
                        y_max = y;
                        line[*numberLine][3] = y_max;
                    }
                    whiteLine = false;
                }
            }
        }
    }

// RESUME LES COORDONNEES

    for (int i = 1; i <= *numberLine; i++)
    {
        printf("| %d. x_min = %d | y_min = %d | x_max = %d | y_max = %d |\n",
                            i, line[i][0], line[i][1], line[i][2], line[i][3]);
    }

// RENDU VISUEL

    for (int w = 1; w <= *numberLine; w++)
    {
        for (int y = line[w][1]; y <= line[w][3]; y++)
        {
            for (int x = line[w][0]; x <= line[w][2]; x++)
            {
                pixel = getPixel(surface, x, y);
                if (x == line[w][0] || x == line[w][2] || y == line[w][1] ||
                                                            y == line[w][3])
                    becomeBlue(surface, x, y, pixel);
            }
        }
    }

    SDL_UnlockSurface(surface);

    return (line);
}

/************************* DETECTION DES CARACTERES ***************************/

void char_detect(SDL_Surface *surface, int **line, int *numberLine, int learnOn)
{
    SDL_LockSurface(surface);

    Uint32 pixel;
    bool borderLeft = false;
    bool borderRight = true;
    bool whiteColumnA = false;
    bool char_stuck = false;
    bool newChar = false;
    int nbBlackPixel = 0;
    int l_ref = 0;
    int stuck_ref = 0;
    int color = 0;
    int average = 0;
    int current_space = 0;
    int k = 0;
    int **matrix = malloc(sizeof(int*));
    size_t sizeX = 0;
    size_t sizeY = 0;
    int *result = malloc(sizeof(int));
    int nbChar = 0;
    char currChar;

    char *test = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567\
                                                                89.,;:!?-*+/";
    Network *n = newNetwork();
    FILE *f = fopen("output.txt", "w+");

// ENCADREMENT DES CARACTERES

    for (int w = 1; w <= *numberLine; w++)
    {
        l_ref = line[w][0];
        char_stuck = false;
        current_space = 0;
        newChar = false;

        average = space_average(surface, line, w);

        for (int x = line[w][0]; x <= line[w][2]; x++)
        {
            newChar = false;
            nbBlackPixel = 0;

            if (borderLeft && pixelColor(pixel, surface) == 255)
                current_space += 1;

            for (int y = line[w][1]; y <= line[w][3]; y++)
            {
                newChar = false;

                pixel = getPixel(surface, x, y);
                if (y != line[w][1] && y != line[w][3] && pixelColor(pixel,
                                                                surface) == 0)
                    nbBlackPixel += 1;

                if (y == line[w][3] && nbBlackPixel > 0)
                    whiteColumnA = false;
                else if (y == line[w][3] && nbBlackPixel == 0)
                whiteColumnA = true;

// BORD DROIT ET EXTRACTION DU CARACTERE

                if (y == line[w][3] && (borderRight || char_stuck) &&
                        !(isWhiteColumnPS(surface, w, (x-1), line)) &&
                                            whiteColumnA && x != line[w][2])
                {
                    newChar = true;

                    if (char_stuck)
                        l_ref = stuck_ref;

                    sizeX = 0;

                    matrix = malloc(sizeof(int*) * (line[w][3] -
                                                            line[w][1] + 1));

                    for (int vertical = line[w][1]; vertical <= line[w][3];
                                                                    vertical++)
                    {
                        sizeY = 0;

                        matrix[sizeX] = malloc(sizeof(int) * (x - l_ref + 1));

                        for (int horizontal = l_ref; horizontal <= x;
                                                                horizontal++)
                        {
                            pixel = getPixel(surface, horizontal, vertical);

                            if (pixelColor(pixel, surface) == 0)
                                color = 1;
                            else
                                color = 0;

                            matrix[sizeX][sizeY] = color;

                            sizeY++;
                        }
                        sizeX++;
                    }

                    for (int i = line[w][1]; i <= line[w][3]; i++)
                    {
                        if (i == line[w][1])
                        {
                            for (int j = l_ref; j <= x; j++)
                            {
                                pixel = getPixel(surface, j, i);
                                becomeGreen(surface, j, i, pixel);
                            }
                        }

                        pixel = getPixel(surface, x, i);
                        becomeGreen(surface, x, i, pixel);

                        if (i == line[w][3])
                        {
                            for (int j = l_ref; j <= x; j++)
                            {
                                pixel = getPixel(surface, j, i);
                                becomeGreen(surface, j, i, pixel);
                            }
                        }
                    }

                    char_stuck = false;
                    borderRight = false;
                    borderLeft = true;

    // SI LES CARACTERES SONT COLLES

                    if (!(isWhiteColumnPS(surface, w, (x+1), line)))
                    {
                        char_stuck = true;
                        stuck_ref = x;
                    }
                }

// BORD GAUCHE ET ESPACES

                else if ((y == line[w][3] && borderLeft && whiteColumnA &&
                !(isWhiteColumnPS(surface, w, (x+1), line))) ||
                                    ((x == line[w][0]) && (y == line[w][1])))
                {
                    for (int i = line[w][1]; i <= line[w][3]; i++)
                    {
                        pixel = getPixel(surface, x, i);
                        becomeGreen(surface, x, i, pixel);
                    }

                    if ((current_space > average) || ((w > 1) &&
                                      (x == line[w][0]) && (y == line[w][1])))
                    {
                        newChar = true;

                        if (x == line[w][0])
                            current_space = average;

                        sizeX = 0;

                        matrix = malloc(sizeof(int*) * (line[w][3] -
                                                            line[w][1] + 1));

                        for (int vertical = line[w][1]; vertical <=
                                                        line[w][3]; vertical++)
                        {
                            sizeY = 0;

                            matrix[sizeX] = malloc(sizeof(int) *
                                                        (current_space + 1));

                            for (int horizontal = 0;
                                    horizontal <= current_space; horizontal++)
                            {
                                matrix[sizeX][sizeY] = 0;

                                sizeY++;
                            }
                            sizeX++;
                        }

                        for (int i = line[w][1]; i <= line[w][3]; i++)
                        {
                            if (i == line[w][1])
                            {
                                for (int j = x - current_space; j <= x; j++)
                                {
                                    pixel = getPixel(surface, j, i);
                                    becomeBlack(surface, j, i, pixel);
                                }
                            }

                            if (i == line[w][3])
                            {
                                for (int j = x - current_space; j <= x; j++)
                                {
                                    pixel = getPixel(surface, j, i);
                                    becomeBlack(surface, j, i, pixel);
                                }
                            }
                        }
                    }

                    borderRight = true;
                    borderLeft = false;
                    l_ref = x;
                    current_space = 0;
                }

// CAS DU DERNIER CARACTERE

                if (x == line[w][2] && y == line[w][1])
                {
                    newChar = true;

                    if (char_stuck)
                        l_ref = stuck_ref;

                    sizeX = 0;

                    matrix = malloc(sizeof(int*) * (line[w][3] -
                                                            line[w][1] + 1));

                    for (int vertical = line[w][1]; vertical <=
                                                        line[w][3]; vertical++)
                    {
                        sizeY = 0;

                        matrix[sizeX] = malloc(sizeof(int) * (x - l_ref + 1));

                        for (int horizontal = l_ref;
                                                horizontal <= x; horizontal++)
                        {
                            pixel = getPixel(surface, horizontal, vertical);

                            if (pixelColor(pixel, surface) == 0)
                                color = 1;
                            else
                                color = 0;

                            matrix[sizeX][sizeY] = color;

                            sizeY++;
                        }
                        sizeX++;
                    }

                    for (int i = line[w][1]; i <= line[w][3]; i++)
                    {
                        if (i == line[w][1])
                        {
                            for (int j = l_ref; j <= x; j++)
                            {
                                pixel = getPixel(surface, j, i);
                                becomeGreen(surface, j, i, pixel);
                            }
                        }

                        pixel = getPixel(surface, x, i);
                        becomeGreen(surface, x, i, pixel);

                        if (i == line[w][3])
                        {
                            for (int j = l_ref; j <= x; j++)
                            {
                                pixel = getPixel(surface, j, i);
                                becomeGreen(surface, j, i, pixel);
                            }
                        }
                    }
                }

                if (newChar)
                {
                    currChar = recognizeChar(n, sizeX, sizeY, matrix, test[k],
                                                                    learnOn);
                    result = realloc(result, sizeof(int) * (nbChar+1));
                    if (currChar == -2)
                        currChar = ' ';
                    else
                    {
                        if (currChar == -1)
                            currChar = '?';

                        if(learnOn)
                        {
                            printf("Expected character: '%c'\n", test[k]);
                            printf("Recognized character: '%c'\n",currChar);
                        }

                        k++;
                    }

                    result[nbChar] = currChar;
                    fprintf(f, "%c", currChar);
                    nbChar++;
                    printf("\n");
                }
            }
        }

        fprintf(f, "\n");
    }

    printf("Output text: (Saved in output.txt)\n");
    for(int i = 0 ; i < nbChar ; i++)
        printf("%c", result[i]);
    printf("\n");

    freeNetwork(n);
    SDL_UnlockSurface(surface);
    fclose(f);
}

/************ MAIN ************/

int main(int argc, char* argv[])
{
    init_sdl();

    if (argc != 0)
    {
        char* img;
        char* output = "characters.bmp";
        char* output1 = "block.bmp";
        char* output2 = "line.bmp";
        int init = 0;
        int tmp = 0;
        int *numberBlock = &tmp;
        int *numberLine = &init;
        img = argv[argc - 1];
        SDL_Surface *image = IMG_Load(img);
        SDL_Surface *image1 = IMG_Load(img);
        SDL_Surface *image2 = IMG_Load(img);
        SDL_Surface *image3 = IMG_Load(img);
        SDL_Surface *image4 = IMG_Load(img);

        display_image(image);
        wait_for_keypressed();

        int **tabH = horizontal_detect(image);
        display_image(image);
        wait_for_keypressed();

        int **tabV = vertical_detect(image1);
        display_image(image1);
        wait_for_keypressed();

        seg_block(image2, tabH, tabV);
        display_image(image2);
        wait_for_keypressed();

        vertical_detect(image2);
        display_image(image2);
        wait_for_keypressed();

        horizontal_detect(image2);
        display_image(image2);
        wait_for_keypressed();

        int **block = block_detect(image2, numberBlock);
        SDL_SaveBMP(image2, output1);
        display_image(image2);
        wait_for_keypressed();

        int **line = line_detect(image3, numberLine, block, numberBlock);
        SDL_SaveBMP(image3, output2);
        display_image(image3);
        wait_for_keypressed();

        char_detect(image4, line, numberLine, (((argc) == (3) ? (1) : (0))));
        SDL_SaveBMP(image4, output);
        display_image(image4);
        wait_for_keypressed();

        SDL_FreeSurface(image);
        SDL_FreeSurface(image1);
        SDL_FreeSurface(image2);
        SDL_FreeSurface(image3);
        SDL_FreeSurface(image4);
    }

    return 0;
}
