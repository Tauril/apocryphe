void grayScale(SDL_Surface *surface);
SDL_Surface *binarizeSauvola(SDL_Surface *surface);
SDL_Surface *createSurface(int x, int y);
SDL_Surface *filter(SDL_Surface *surface);
Uint32 getPixel(SDL_Surface *surface, int x, int y);
void setPixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
void lockSurface(SDL_Surface *surface);
void unlockSurface(SDL_Surface *surface);
