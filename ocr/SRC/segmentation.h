Uint32 getPixel(SDL_Surface *surface, int x, int y);
void setPixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
int pixelColor(Uint32 pixel, SDL_Surface *surface);
void becomeBlack(SDL_Surface *surface, int x, int y, Uint32 pixel);
void becomeWhite(SDL_Surface *surface, int x, int y, Uint32 pixel);
void becomeGreen(SDL_Surface *surface, int x, int y, Uint32 pixel);
void becomeBlue(SDL_Surface *surface, int x, int y, Uint32 pixel);
int space_average_seg_x(SDL_Surface *surface, int y);
int space_average_seg_y(SDL_Surface *surface, int x);
void seg_block(SDL_Surface *surface, int **tabH, int **tabV);
int **horizontal_detect(SDL_Surface *surface);
int **vertical_detect(SDL_Surface *surface);
int **block_detect(SDL_Surface *surface, int *numberBlock);
int *rec_coord(SDL_Surface *surface, int x, int y, int *coord, int **tab);
int **line_detect(SDL_Surface *surface, int *numberLine, int **block, int
                                                                *numberBlock);
void char_detect(SDL_Surface *surface, int **line, int *numberLine, int
																learnOn);
int isWhiteColumnPS(SDL_Surface *surface, int w, int x, int **line);
int space_average(SDL_Surface *surface, int **line, int numberLine);
void wait_for_keypressed(void);
void init_sdl(void);
SDL_Surface* display_image(SDL_Surface *img);
