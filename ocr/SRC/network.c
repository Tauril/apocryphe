/******************************* INCLUDES *************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/******************************** DEFINES *************************************/

#define SIZE			256
#define LEARNING_COEF	1
#define BIAIS			0.5
#define ERROR			1
#define NB_ITER			1000

/******************************* STRUCTURES ***********************************/

//Structure d'un neurone.
typedef struct Neurone
{
	float input;
	float *weights;
	int result;

}Neurone;

//Structure d'une couche de neurones.
typedef struct Layer
{
	Neurone **neurones;

}Layer;

//Structure d'un réseau composé de 3 couches (entrée, cachée, sortie).
typedef struct Network
{
	Layer *inLayer, *hidLayer, *outLayer;

}Network;

/****************************** SERIALIZATION *********************************/

//Clean le fichier de sauvegarde des poids.
void clearFile()
{
	FILE *f = fopen("weights", "w+");
	if (f)
		fclose(f);
}

//Sauvegarde les poids des neurones d'une couche.
void saveWeightsLayer(Layer *l)
{
	FILE *f = fopen("weights", "a");
	if (f != NULL)
	{
		for(size_t i = 0; i < SIZE; i++)
			for(size_t j = 0; j < SIZE; j++)
				fprintf(f, "%f\n", l->neurones[i]->weights[j]);
	}
	fclose(f);
}

//Sauvegarde les poids.
void saveWeights(Network *n)
{
	clearFile();
	saveWeightsLayer(n->inLayer);
	saveWeightsLayer(n->hidLayer);
}

//Récupère les poids sauvegardés et les attributs aux neurones de la couche.
void getWeights(Network *n)
{
	FILE *f = fopen("weights", "r");
	if (f != NULL)
	{
		for(size_t i = 0; i < SIZE; i++)
			for(size_t j = 0; j < SIZE &&
					fscanf(f, "%f", &(n->inLayer->neurones[i]->weights[j]));
					j++) {}

		for(size_t i = 0; i < SIZE; i++)
			for(size_t j = 0; j < SIZE &&
					fscanf(f, "%f", &(n->hidLayer->neurones[i]->weights[j]));
					j++) {}
		fclose(f);
	}
}

/********************************* NEURONE ************************************/

//Rempli aléatoirement le vecteur de poids entre 0 et 1.
void randomWeights(Neurone *n)
{
	srand(time(NULL));
	for (size_t i = 0; i < SIZE; i++)
		n->weights[i] = ((float)rand() / (float)RAND_MAX);
}

//Créée et initialise un neurone.
Neurone *newNeurone()
{
	Neurone *n = malloc(sizeof(Neurone));
	n->input = 0;
	n->weights = malloc(sizeof(float) * SIZE);
	randomWeights(n);
	n->result = 0;
	return n;
}

//Libère la place mémoire d'un neurone.
void freeNeurone(Neurone *n)
{
	free(n->weights);
	free(n);
}

/********************************* LAYER **************************************/

//Créée et initialise une couche de neurones.
Layer *newLayer()
{
	Layer *l = malloc(sizeof(Layer));
	l->neurones = malloc(sizeof(Neurone*) * SIZE);
	for (size_t i = 0; i < SIZE; i++)
		l->neurones[i] = newNeurone();
	return l;
}

//Initialise une valeur escomptée pour tous les neurones d'une couche.
void setResultLayer(Layer *l, int result)
{
	for(int i = 0; i < SIZE; i++)
		l->neurones[i]->result = (((result) == (i)) ? (1) : (0));
}


//Rééquilibre les poids de tous les neurones d'une couche.
void rebalanceWeights(Layer *src, Neurone *dst, size_t i)
{
	for(size_t k = 0; k < SIZE; k++)
	{
		src->neurones[k]->weights[i] +=
			LEARNING_COEF * (dst->result - dst->input) *
			src->neurones[k]->input;
	}

}

void rebalanceWeightsNetwork(Network *n)
{
	/*for(size_t i = 0; i < SIZE; i++)
	{
		rebalanceWeights(n->inLayer, n->hidLayer->neurones[i], i);
		rebalanceWeights(n->hidLayer, n->outLayer->neurones[i], i);
	}*/

	float hid = 0, *out, sumHid;
	out = malloc(sizeof(float) * SIZE);

	//Rétropropagation sur la couche de sortie.
	for(size_t i = 0; i < SIZE; i++)
	{
		out[i] = n->outLayer->neurones[i]->input *
			(1 - n->outLayer->neurones[i]->input) *
			(n->outLayer->neurones[i]->result -
			n->outLayer->neurones[i]->input);

	  for(size_t j = 0; j < SIZE; j++)
			n->hidLayer->neurones[j]->weights[i] +=
				LEARNING_COEF * out[i] * n->hidLayer->neurones[j]->input;
	}

	//Rétropropagation sur la couche cache
	for(size_t i = 0; i < SIZE; i++)
	{
		sumHid = 0;
		for(size_t j = 0; j < SIZE; j++)
		sumHid += out[j] * n->inLayer->neurones[i]->weights[j];
		hid = sumHid * n->hidLayer->neurones[i]->input *
		(1 - n->hidLayer->neurones[i]->input);

		for(size_t j = 0; j < SIZE; j++)
			n->inLayer->neurones[j]->weights[i] +=
			LEARNING_COEF * hid * n->inLayer->neurones[j]->input;
	}
}

//Calcule l'erreur quadratique sur une couche.
float quadraticError(Layer *l)
{
	float out = 0;
	for(int i = 0; i < SIZE; i++)
		out += powf(l->neurones[i]->result - l->neurones[i]->input, 2);
	return out /= 2;
}

//Binarise la valeur des neurones d'une couche en fonction du biais.
void binarizeInputs(Layer *l)
{
	for(size_t i = 0; i < SIZE; i++)
		l->neurones[i]->input =
			(((l->neurones[i]->input) >= (0.5)) ? (1) : (0));
}

//Active ou non tous les neurones d'une couche.
void activateLayer(Layer *src, Layer *dst)
{
	float weightedSum;

	for (size_t i = 0; i < SIZE; i++)
	{
		weightedSum = 0;

		//Calcul de la somme pondérée.
		for (size_t j = 0; j < SIZE; j++)
			weightedSum += src->neurones[j]->input *
                                                   src->neurones[j]->weights[i];

		//Application de la sigmoïde sur la somme pondérée.
		dst->neurones[i]->input = 1 / (1 + exp(-weightedSum));
	}
}

//Libère la place mémoire d'une couche de neurones.
void freeLayer(Layer *l)
{
	for (size_t i = 0; i < SIZE; i++)
		freeNeurone(l->neurones[i]);
	free(l->neurones);
	free(l);
}

/********************************* NETWORK ************************************/

//Créée et initialise un réseau de neurones.
Network *newNetwork()
{
	Network *n = malloc(sizeof(Network));
	n->inLayer = newLayer();
	n->hidLayer = newLayer();
	n->outLayer = newLayer();
	return n;
}

//Initialise un résultat escompté pour l'apprentissage du réseau.
void setResult(Network *n, int result)
{
	setResultLayer(n->hidLayer, result);
	setResultLayer(n->outLayer, result);
}

//Initialise les valeurs des neurones de la couche d'entrée de réseau.
void setInputs(Network *n, int *inputs)
{
	for(size_t i = 0; i < SIZE; i++)
		n->inLayer->neurones[i]->input = inputs[i];
}

//Lance l'execution du réseau de neurones.
int runNetwork(Network *n)
{
	int r = -1;
	float out = 0;
	getWeights(n);

	activateLayer(n->inLayer, n->hidLayer);
	activateLayer(n->hidLayer, n->outLayer);

	for(int i = 0; i < SIZE; i++)
	{
		if (n->outLayer->neurones[i]->input > out)
		{
			out = n->outLayer->neurones[i]->input;
			r = i;
		}
	}

	printf("Recognized character: '%c'\n", r);
	return r;
}

//Lance l'apprentissage du réseau de neurones.
int learnNetwork(Network *n, int result)
{
	setResult(n, result);
	getWeights(n);

	int i = NB_ITER;
	int r = -1;
	float out = 0;

	do
	{
		activateLayer(n->inLayer, n->hidLayer);
		activateLayer(n->hidLayer, n->outLayer);
		rebalanceWeightsNetwork(n);

	} while (/*quadraticError(n->outLayer) >= ERROR ||*/ i-- > 0);

	saveWeights(n);

	for(int i = 0; i < SIZE; i++)
	{
		if (n->outLayer->neurones[i]->input > out)
		{
			out = n->outLayer->neurones[i]->input;
			r = i;
		}
	}

	return r;
}

//Libère la mémoire d'un réseau de neurones.
void freeNetwork(Network *n)
{
	freeLayer(n->inLayer);
	freeLayer(n->hidLayer);
	freeLayer(n->outLayer);
	free(n);
}
