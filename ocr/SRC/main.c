#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include "image.h"
#include "rotate.h"

static void usage(int option)
{
    if(option == 0)
        printf("Usage: ./Pretreatment [-bfg] [-a] [-r angle] "
               "image [-o output]\n");
    else
        printf("File does not exist\n");
    exit(1);
}

int main(int argc, char* argv[])
{
    char* img;
    char* output = "result.bmp";
    int angle = 0;
    int aflag = 0;
    int bflag = 0;
    int fflag = 0;
    int gflag = 0;
    int errflg = 0;
    int rflag = 0;
    int c;

    struct option long_opt[] =
    {
        {"help",        no_argument,        NULL, 'h'},
        {"binarize",    no_argument,        NULL, 'b'},
        {"grayscale",   no_argument,        NULL, 'g'},
        {"filter",      no_argument,        NULL, 'f'},
        {"output",      required_argument,  NULL, 'o'}
    };

    while ((c = getopt_long(argc, argv, "abfgho:r:", long_opt, NULL)) != -1)
    {
        switch (c)
        {
            case 'a':
                aflag = 1;
                break;
            case 'b':
                bflag = 1;
                break;
            case 'f':
                fflag = 1;
                break;
            case 'g':
                gflag = 1;
                break;
            case 'h':
                usage(0);
            case 'o':
                output = optarg;
                break;
            case 'r':
                rflag = 1;
                angle = atoi(optarg);
                break;
            case '?':
                errflg++;
                break;
            default:
                usage(0);
        }
    }
    if(errflg || argc == optind)
        usage(0);
    /*if(optind < argc)*/
        img = argv[optind];
    if(access(img, F_OK) == -1)
        usage(1);

    SDL_Surface *image = IMG_Load(img);

    if(gflag || aflag)
        grayScale(image);
    if(fflag || aflag)
        image = filter(image);
    if(bflag || aflag)
        image = binarizeSauvola(image);
    if(rflag)
        image = rotate(image, angle);

    if(!(aflag || bflag || fflag || gflag || rflag))
        printf("You didn't set any option, the image won't be modified.\n");

    SDL_SaveBMP(image, output);
    printf("Saving image to %s\n", output);

    return 0;
}
