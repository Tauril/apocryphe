#include <stdlib.h>
#include <stdio.h>

#define SIZE 16

#ifndef _NETWORK_
#define _NETWORK_

typedef struct Neurone
{
	int input;
	float *weights;
	int result;
}Neurone;

typedef struct Layer
{
	Neurone **neurones;
}Layer;

typedef struct Network
{
	Layer *inLayer, *hidLayer, *outLayer;
}Network;

//Rempli aléatoirement le vecteur de poids entre 0 et 1.
void randomWeights(Neurone *n);

//Créée et initialise un neurone.
Neurone *newNeurone();

//Libère la place mémoire d'un neurone.
void freeNeurone(Neurone *n);

//Créée et initialise une couche de neurones.
Layer *newLayer();

//Initialise une valeur escomptée pour tous les neurones d'une couche.
void setResultLayer(Layer *l, int result);

//Rééquilibre les poids de tous les neurones d'une couche.
void rebalanceWeights(Layer *src, Neurone *dst, size_t i);

//Active ou non tous les neurones d'une couche.
void activateLayer(Layer *src, Layer *dst);

//Libère la place mémoire d'une couche de neurones.
void freeLayer(Layer *l);

//Créée et initialise un réseau de neurones.
Network *newNetwork();

//Initialise un résultat escompté pour l'apprentissage du réseau.
void setResult(Network *n, int result);

//Initialise les valeurs des neurones de la couche d'entrée de réseau.
void setInputs(Network *n, int *inputs);

//Lance l'execution du réseau de neurones.
int runNetwork(Network *n);

//Lance l'apprentissage du réseau de neurones.
int learnNetwork(Network *n, int result);

//Libère la mémoire d'un réseau de neurones.
void freeNetwork(Network *n);

//Sauvegarde les poids
void saveWeights(Network *n);

#endif
