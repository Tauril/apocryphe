#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <math.h>
#include "image.h"

int pixelColor(Uint32 pixel, SDL_Surface *surface)
{
    SDL_LockSurface(surface);
	Uint8 r,g,b;
	SDL_GetRGB(pixel, surface->format, &r, &g, &b);

	if (r < 224 && g < 224 && b < 224)
	{
		SDL_UnlockSurface(surface);
		return 0;
	}
	else
	{
		SDL_UnlockSurface(surface);
		return 255;
	}
}

SDL_Surface *rotate(SDL_Surface *image, int angle)
{
	//Trigo
	double c = cos(3.14159*angle/180);
	double s = sin(3.14159*angle/180);

	//Original Dimensions
	int w = image->w;
	int h = image->h;

	//New dimensions
	int wo = w;//abs(w*c + h*s);
	int ho = h;//abs(h*c + w*s);

	SDL_Surface *new_img;
	Uint32 rmask, gmask, bmask, amask;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;
#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
#endif
	new_img = SDL_CreateRGBSurface(0,wo,ho,32, rmask, gmask, bmask, amask);

	//Middle coordinates
	int ocx = w/2;
	int ocy = h/2;

	//New middle coordinates
	int ncx = wo/2;
	int ncy = ho/2;

	Uint8 r,g,b;
	Uint32 pixel;
	Uint32 color;

	lockSurface(image);
	lockSurface(new_img);
	for (int y = 0; y < ho; y++)
	{
		for(int x = 0; x < wo; x++)
		{
			//New coordinates
			int xo = ocx + (c*(x - ncx) + s*(y - ncy));
			int yo = ocy + (c*(y - ncy) - s*(x - ncx));

			if (xo >= 0 && yo >= 0 && xo < image->w && yo < image->h)
			{
				pixel = getPixel(image, xo, yo);
				SDL_GetRGB(pixel,image->format,&r,&g,&b);
				color = SDL_MapRGB(image->format,r,g,b);
				setPixel(new_img,x,y,color);
			}

			else
			{
				color = SDL_MapRGB(new_img->format, 255, 255, 255);
				setPixel(new_img, x, y, color);
			}
		}
	}
	unlockSurface(image);
	unlockSurface(new_img);
	return new_img;
}
